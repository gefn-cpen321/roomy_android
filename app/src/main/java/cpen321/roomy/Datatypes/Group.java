package cpen321.roomy.Datatypes;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.Vector;

public class Group {
    public List<String> users;
    public String messagesRef;
    public String tasksRef;
    public String billsRef;

    public Group() {
        users = new Vector<>();
        messagesRef = "";
        tasksRef = "";
        billsRef = "";
    }

    public boolean addUserToGroup(@NonNull String userid) {
        if (userid.isEmpty()) {
            return false;
        }
        if (users.contains(userid)) {
            return false;
        }

        users.add(userid);
        return true;
    }

    public boolean isEmpty() {
        return users.isEmpty();
    }

    public boolean removeUserFromGroup(@NonNull String userid) {
        if (userid.isEmpty()) {
            return false;
        }
        if (!users.contains(userid)) {
            return false;
        }

        users.remove(userid);
        return true;
    }
}
