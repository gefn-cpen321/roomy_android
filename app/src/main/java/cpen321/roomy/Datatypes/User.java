package cpen321.roomy.Datatypes;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
    public String name;
    public String email;
    public String groupid;
    public String profile;
    public final Boolean admin = false;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
        this.name = "";
        this.email = "";
        this.groupid = "";
        this.profile = "";
    }

    public User(String name, String email, String groupId, String profilePicture) {
        this.name = name;
        this.email = email;
        this.groupid = groupId;
        this.profile = profilePicture;
    }
}
