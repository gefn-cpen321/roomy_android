package cpen321.roomy;

import android.os.Bundle;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditSettingsDialog extends DialogFragment
{

    public interface EditSettingsListener {
        void onSettingsChanged(String settings);
    }

    private EditSettingsListener listener;
    private String title = "Dialog";

    private Button buttonOk;
    private Button buttonCancel;
    private EditText editText;
    private TextView textDesc;

    public void setTitle(String title) {
        this.title = title;
    }

    private final View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.dialog_button_ok) {
                listener.onSettingsChanged(editText.getText().toString());
            }
            dismiss();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_edit_settings, container);

        buttonOk = view.findViewById(R.id.dialog_button_ok);
        buttonCancel = view.findViewById(R.id.dialog_button_cancel);
        editText = view.findViewById(R.id.dialog_edit);
        textDesc = view.findViewById(R.id.dialog_text_description);

        textDesc.setText(title);
        buttonOk.setOnClickListener(buttonListener);
        buttonCancel.setOnClickListener(buttonListener);

        return view;
    }

    public void addEditSettingsListener(EditSettingsListener listener) {
        this.listener = listener;
    }
}
