package cpen321.roomy.Fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import cpen321.roomy.Datatypes.Group;
import cpen321.roomy.Datatypes.User;
import cpen321.roomy.EditSettingsDialog;
import cpen321.roomy.MainActivity;
import cpen321.roomy.R;
import cpen321.roomy.Utils.Authentication;

public class SettingsFragment extends Fragment {

    // Firebase database fields
    private DatabaseReference mDatabaseRefUser;
    private DatabaseReference mDatabaseRefGroup;

    private String mUserId;
    private User mUser;
    private HashMap<String, Group> mGroups;


    // Fields for layout widgets
    private Button buttonCreateGroup;
    private Button buttonLeaveGroup;
    private Button buttonJoinGroup;
    private Button buttonSignOut;

    private ImageView imgEditName;
    private ImageView imgEditEmail;
    private ImageView imgCopyGid;

    private TextView textName;
    private TextView textEmail;
    private TextView textGid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mUserId = Authentication.getInstance().getUid();
        assert(mUserId != null);
        mGroups = new HashMap<>();

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        // Setup all database connections
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        mDatabaseRefUser = ref.child("users").child(mUserId);
        mDatabaseRefGroup = ref.child("groups");

        mDatabaseRefUser.addValueEventListener(mValueEventListenerUser);
        mDatabaseRefGroup.addValueEventListener(mValueEventListenerGroup);

        initUI(rootView);
        initListeners();
        return rootView;
    }

    private void initUI(View view) {
        buttonCreateGroup = view.findViewById(R.id.settings_button_create_group);
        buttonLeaveGroup  = view.findViewById(R.id.settings_button_leave_group);
        buttonJoinGroup   = view.findViewById(R.id.settings_button_join_group);
        buttonSignOut     = view.findViewById(R.id.settings_button_sign_out);
        buttonLeaveGroup.setVisibility(View.GONE);

        imgEditName  = view.findViewById(R.id.settings_edit_name);
        imgEditEmail = view.findViewById(R.id.settings_edit_email);
        imgCopyGid   = view.findViewById(R.id.settings_copy_group);

        textName  = view.findViewById(R.id.settings_name);
        textEmail = view.findViewById(R.id.settings_email);
        textGid   = view.findViewById(R.id.settings_group);
    }

    private void initListeners() {
        imgEditName.setOnClickListener(mOnClickListenerEditName);
        imgEditEmail.setOnClickListener(mOnClickListenerEditEmail);
        imgCopyGid.setOnClickListener(mOnClickListenerCopyGid);

        buttonCreateGroup.setOnClickListener(mOnClickListenerCreateGroup);
        buttonLeaveGroup.setOnClickListener(mOnClickListenerLeaveGroup);
        buttonJoinGroup.setOnClickListener(mOnClickListenerJoinGroup);
        buttonSignOut.setOnClickListener(mOnClickListenerSignOut);
    }

    //=========================================================================
    // Database ValueEventListener definitions
    //=========================================================================
    private final ValueEventListener mValueEventListenerUser = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            mUser = dataSnapshot.getValue(User.class);
            if (mUser == null) {
                return;
            }

            textName.setText(mUser.name);
            textEmail.setText(mUser.email);

            if (mUser.groupid.isEmpty()) {
                textGid.setText(getString(R.string.settings_hint_group_id));
                buttonCreateGroup.setVisibility(View.VISIBLE);
                buttonLeaveGroup.setVisibility(View.GONE);
                buttonJoinGroup.setText(getString(R.string.settings_button_join));
            } else {
                textGid.setText(mUser.groupid);
                buttonCreateGroup.setVisibility(View.GONE);
                buttonLeaveGroup.setVisibility(View.VISIBLE);
                buttonJoinGroup.setText(R.string.settings_button_join_other);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private final ValueEventListener mValueEventListenerGroup = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot node : dataSnapshot.getChildren()) {
                String gid = node.getKey();

                if (!mGroups.containsKey(gid)) {
                    mGroups.put(gid, node.getValue(Group.class));
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    //=========================================================================
    // UI OnClickListener definitions
    //=========================================================================
    private final View.OnClickListener mOnClickListenerEditName = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EditSettingsDialog dialog = new EditSettingsDialog();
            dialog.setTitle("Name");

            dialog.addEditSettingsListener(new EditSettingsDialog.EditSettingsListener() {
                @Override
                public void onSettingsChanged(String settings) {
                    if (settings.length() < 2) {
                        Toast.makeText(getActivity(),
                                "Your name must be at least two characters long",
                                Toast.LENGTH_LONG).show();
                        return;
                    }

                    textName.setText(settings);
                    mUser.name = settings;
                    mDatabaseRefUser.setValue(mUser);
                }
            });
            dialog.show(getFragmentManager(), "dialog");
        }
    };

    private final View.OnClickListener mOnClickListenerEditEmail = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EditSettingsDialog dialog = new EditSettingsDialog();
            dialog.setTitle("Email");
            dialog.addEditSettingsListener(new EditSettingsDialog.EditSettingsListener() {
                @Override
                public void onSettingsChanged(String settings) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(settings).matches()) {
                        Toast.makeText(getActivity(),
                                "Your email is invalid",
                                Toast.LENGTH_LONG).show();
                        return;
                    }

                    textEmail.setText(settings);
                    mUser.email = settings;
                    mDatabaseRefUser.setValue(mUser);
                }
            });
            dialog.show(getFragmentManager(), "dialog");
        }
    };

    private final View.OnClickListener mOnClickListenerCopyGid = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText("Group ID", textGid.getText().toString());

            if (clipboard != null) {
                clipboard.setPrimaryClip(clipData);
            }
        }
    };

    private final View.OnClickListener mOnClickListenerCreateGroup = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mUser == null || !mUser.groupid.isEmpty()) {
                return;
            }

            Group group = new Group();
            group.addUserToGroup(mUserId);

            mUser.groupid = mDatabaseRefGroup.push().getKey();
            mDatabaseRefGroup.child(mUser.groupid).setValue(group);
            mDatabaseRefUser.setValue(mUser);
        }
    };

    private final View.OnClickListener mOnClickListenerLeaveGroup = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mUser == null || mUser.groupid.isEmpty()) {
                return;
            }

            Group group = mGroups.get(mUser.groupid);
            group.removeUserFromGroup(mUserId);

            if (group.users.isEmpty()) {
                mGroups.remove(mUser.groupid);
                mDatabaseRefGroup.child(mUser.groupid).removeValue();
            } else {
                mGroups.put(mUser.groupid, group);
                mDatabaseRefGroup.child(mUser.groupid).setValue(group);
            }

            mUser.groupid = "";
            mDatabaseRefUser.setValue(mUser);
        }
    };

    private final View.OnClickListener mOnClickListenerJoinGroup = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EditSettingsDialog dialog = new EditSettingsDialog();
            dialog.setTitle("Group");

            dialog.addEditSettingsListener(new EditSettingsDialog.EditSettingsListener() {
                @Override
                public void onSettingsChanged(String settings) {
                    String oldGroupId = mUser.groupid;
                    if (mUser == null || !mGroups.containsKey(settings)) {
                        return;
                    }

                    if (!mUser.groupid.isEmpty()) {
                        Group oldGroup = mGroups.get(oldGroupId);
                        oldGroup.removeUserFromGroup(mUserId);

                        if (oldGroup.users.isEmpty()) {
                            mGroups.remove(oldGroupId);
                            mDatabaseRefGroup.child(oldGroupId).removeValue();
                        } else {
                            mGroups.put(mUser.groupid, oldGroup);
                            mDatabaseRefGroup.child(oldGroupId).setValue(oldGroup);
                        }
                    }

                    Group newGroup = mGroups.get(settings);
                    newGroup.addUserToGroup(mUserId);
                    mGroups.put(settings, newGroup);
                    mDatabaseRefGroup.child(settings).setValue(newGroup);

                    mUser.groupid = settings;
                    mDatabaseRefUser.setValue(mUser);
                }
            });
            dialog.show(getFragmentManager(), "dialog");

        }
    };

    private final View.OnClickListener mOnClickListenerSignOut = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Authentication auth = Authentication.getInstance();
            auth.signOut();

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            SettingsFragment myFragment = new SettingsFragment();
            transaction.remove(myFragment).commit();
            ((MainActivity) getActivity()).startAuthActivities();
        }
    };
}
