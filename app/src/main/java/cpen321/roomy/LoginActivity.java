package cpen321.roomy;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import cpen321.roomy.Enums.IntentRequests;
import cpen321.roomy.Utils.Authentication;


public class LoginActivity extends AppCompatActivity {
    // Debugging related fields
    private static final String TAG = "LoginActivity_DEBUG";

    // Authentication fields
    private Authentication auth;

    // Layout widget fields
    private Button buttonLogin;
    private EditText textEmail;
    private EditText textPassword;
    private TextView linkSignup;

    // Authentication listeners
    private final FirebaseAuth.AuthStateListener stateListener =
            new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    auth.refresh(); // Update the current user
                    if (auth.getUid() != null) {
                        Log.d(TAG, "Successfully logged in");
                        setResult(RESULT_OK, null);
                        finish();
                    }
                }
            };
    private final OnCompleteListener<AuthResult> authCompleteListener =
            new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "login with email/pwd: SUCCESS");
                    } else {
                        Exception e = task.getException();
                        if (e == null) {
                            return;
                        }

                        Log.w(TAG, "login with email/pwd: FAILED");
                        Toast.makeText(LoginActivity.this,
                                e.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Initialize authentication
        auth = Authentication.getInstance();
        auth.addAuthStateListener(stateListener);
        auth.setSignInOnCompleteListener(authCompleteListener);

        // Initialize layout widget fields
        buttonLogin = findViewById(R.id.login_button);
        textEmail = findViewById(R.id.login_email);
        textPassword = findViewById(R.id.login_password);
        linkSignup = findViewById(R.id.login_signup_link);

        // OnClickListener for the login button.
        //
        // Sends relevant information to teh Firebase authentication server
        // and verifies this information. On successful verification, users
        // are taken to the home page. Otherwise, they stay on the login page
        // and are told their login attempt has failed.
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = textEmail.getText().toString();
                String password = textPassword.getText().toString();

                // Do not proceed to authentication if email or password are
                // not provided. Don't connect to server if not necessary.
                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(LoginActivity.this,
                            "Please enter your email and password",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                email = textEmail.getText().toString();
                password = textPassword.getText().toString();
                auth.signInWithEmailAndPassword(email, password);
            }
        });

        // OnClickListener for the signup button.
        //
        // Starts the SignupActivity which will allow a user to sign up for
        // Roomy via the Firebase authentication server. On successful singup,
        // the user will be taken back to this activity and automatically
        // logged in, taking them to the home page.
        linkSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Moving to a different authentication action, so remove our
                // current authentication action's state listener.
                auth.removeAuthStateListener();

                // Forward the input email to make account creation easier
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                intent.putExtra("email", textEmail.getText().toString());
                startActivityForResult(intent, IntentRequests.AUTH_REQUEST.ordinal());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IntentRequests.AUTH_REQUEST.ordinal()) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, null);
                finish();
            } else {
                // Cancelled sign up. Because we've come back to this
                // authentication action, reset the state listener.
                auth.addAuthStateListener(stateListener);
            }
        }
    }

    @Override
    public void finish() {
        auth.removeAuthStateListener();
        super.finish();
    }
}