package cpen321.roomy;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import cpen321.roomy.Fragments.*;
import cpen321.roomy.Enums.IntentRequests;
import cpen321.roomy.Utils.Authentication;
import cpen321.roomy.Utils.BottomNavigationViewHelper;

public class MainActivity extends AppCompatActivity {
    // Debugging related fields
    private static final String TAG = "MainActivity_DEBUG";

    // Fragments
    private Fragment settingFragment;
    private Fragment billboardFragment;
    private Fragment messagesFragment;
    private Fragment tasksFragment;
    private Fragment paymentsFragment;

    // Authentication fields
    private Authentication mAuth;

    // Widget fields
    private BottomNavigationView bottomNavigationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeFragments();
        initializeNavigationView();

        mAuth = Authentication.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mAuth.getUid() == null) {
            // No user currently logged in. Start LoginActivity so that a
            // user can log in an use the app.
            startAuthActivities();
        } else {
            bottomNavigationView.setSelectedItemId(R.id.bottom_navigation_menu_billboard);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Received result from LoginActivity");

        if (requestCode == IntentRequests.AUTH_REQUEST.ordinal()) {
            if (resultCode == RESULT_OK) {
                // User has been logged in successfully. We can now initialize
                // the database and do final MainActivity setup
                bottomNavigationView.setSelectedItemId(R.id.bottom_navigation_menu_billboard);
            } else {
                Log.w(TAG, "Left LoginActivity without logging anyone in");
                Log.d(TAG, "Restarting LoginActivity");
                // Somehow, we've left the login page without actually logging
                // anyone in. Restart LoginActivity.
                startAuthActivities();
            }
        }
    }

    private void initializeFragments() {
        settingFragment = new SettingsFragment();
        billboardFragment = new BillboardFragment();
        messagesFragment = new MessageFragment();
        tasksFragment = new TasksFragment();
        paymentsFragment = new PaymentsFragment();
    }

    private void initializeNavigationView(){
        // Modify bottom navigation menu so that it is not in shift mode (i.e. we can see all icon
        // names instead of only the name of the selected item)
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.bottom_navigation_menu_billboard:
                                Log.d(TAG, "Clicked Billboard");
                                setFragment(billboardFragment);
                                break;

                            case R.id.bottom_navigation_menu_messages:
                                Log.d(TAG, "Clicked Messages");
                                setFragment(messagesFragment);
                                break;

                            case R.id.bottom_navigation_menu_tasks:
                                Log.d(TAG, "Clicked Tasks");
                                setFragment(tasksFragment);
                                break;

                            case R.id.bottom_navigation_menu_payments:
                                Log.d(TAG, "Clicked Payments");
                                setFragment(paymentsFragment);
                                break;

                            case R.id.bottom_navigation_menu_settings:
                                Log.d(TAG, "Clicked Menu");
                                setFragment(settingFragment);
                                break;

                            default:
                                Log.d(TAG, "Unrecognized item id " + item.getItemId());
                                setFragment(billboardFragment);
                                break;
                        }
                        return true;
                    }
                });
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    public void startAuthActivities() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivityForResult(intent, IntentRequests.AUTH_REQUEST.ordinal());
    }
}