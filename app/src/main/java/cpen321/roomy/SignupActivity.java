package cpen321.roomy;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import cpen321.roomy.Datatypes.User;
import cpen321.roomy.Utils.Authentication;

public class SignupActivity extends AppCompatActivity {
    // Debugging fields
    private static final String TAG = "SignupActivity_DEBUG";

    private Authentication mAuth;
    private DatabaseReference mDatabaseRefUser;

    // Layout widget fields
    private Button buttonSignup;
    private EditText textName;
    private EditText textEmail;
    private EditText textPassword;

    // Authentication listeners
    private final FirebaseAuth.AuthStateListener stateListener =
            new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    mAuth.refresh(); // Update the current user
                    if (mAuth.getUid() != null) {
                        String name = textName.getText().toString();
                        String email = textEmail.getText().toString();

                        User user = new User(name, email, "", "");
                        mDatabaseRefUser.child(mAuth.getUid()).setValue(user);

                        Log.d(TAG, "Successfully created account for " + textEmail.getText().toString());
                        setResult(RESULT_OK, null);
                        finish();
                    }
                }
            };

    private final OnCompleteListener<AuthResult> authCompleteListener =
            new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "create user with email/pwd: SUCCESS");
                    } else {
                        Exception e = task.getException();
                        if (e == null) {
                            return;
                        }

                        Log.w(TAG, "create user with email/pwd: FAILED");
                        Toast.makeText(SignupActivity.this,
                                e.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        // Initialize authentication
        mAuth = Authentication.getInstance();
        mAuth.addAuthStateListener(stateListener);
        mAuth.setSignUpOnCompleteListener(authCompleteListener);
        mDatabaseRefUser = FirebaseDatabase.getInstance().getReference().child("users");

        // Initialize layout widget fields
        buttonSignup = findViewById(R.id.signup_button);
        textName = findViewById(R.id.signup_name);
        textEmail = findViewById(R.id.signup_email);
        textPassword = findViewById(R.id.signup_password);

        // Transfer info from intent extras to the appropriate layout widgets
        forwardExtras(getIntent().getExtras());

        // OnClickListener for the signup button
        //
        // Attempt to create an account given the credentials
        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validCredentials()) {
                    return;
                }

                String email = textEmail.getText().toString();
                String password = textPassword.getText().toString();
                mAuth.signUpWithEmailAndPassword(email, password);
            }
        });
    }

    @Override
    public void finish() {
        mAuth.removeAuthStateListener();
        super.finish();
    }

    private void forwardExtras(Bundle extras) {
        if (extras == null) {
            return;
        }

        String email = extras.getString("email");
        if (email != null && !email.isEmpty()) {
            Log.d(TAG, "Forwarded email from login activity: " + email);
            textEmail.setText(extras.getString("email"));
        }
    }

    private boolean validCredentials() {
        String name = textName.getText().toString();
        String email = textEmail.getText().toString();
        String password = textPassword.getText().toString();

        // Name validation
        if (name.isEmpty()) {
            Toast.makeText(this, "Please enter your name", Toast.LENGTH_LONG).show();
            return false;
        } else if (name.length() < 2) {
            Toast.makeText(this,
                    "Your name must be at least 2 characters long",
                    Toast.LENGTH_LONG).show();
            return false;
        }

        // Email validation
        if (email.isEmpty()) {
            Toast.makeText(this, "Please enter your email", Toast.LENGTH_LONG).show();
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "You must enter a valid email", Toast.LENGTH_LONG).show();
            return false;
        }

        // Password validation
        if (password.isEmpty()) {
            Toast.makeText(this, "Please enter a password", Toast.LENGTH_LONG).show();
            return false;
        } else if (password.length() < 8) {
            Toast.makeText(this,
                    "Your password must be at least 8 characters long",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}