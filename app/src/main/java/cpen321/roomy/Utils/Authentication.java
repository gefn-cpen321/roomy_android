package cpen321.roomy.Utils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Authentication {
    // Singleton instance
    private static Authentication mInstance;

    // Firebase authentication
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    // Listeners for authentication methods
    private FirebaseAuth.AuthStateListener mStateListener;
    private OnCompleteListener<AuthResult> mListenerSignIn;
    private OnCompleteListener<AuthResult> mListenerSignUp;


    private Authentication() {
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
    }

    public static Authentication getInstance() {
        if (mInstance == null) {
            mInstance = new Authentication();
        }
        return mInstance;
    }

    public String getUid() {
        if (mUser != null) {
            return mUser.getUid();
        } else {
            return null;
        }
    }

    public void refresh() {
        mUser = mAuth.getCurrentUser();
    }

    public void signOut() {
        mAuth.signOut();
        mUser = null;
    }

    //=================================================================
    // Methods to set up FirebaseAuth listeners
    //=================================================================
    public void addAuthStateListener(FirebaseAuth.AuthStateListener listener) {
        mStateListener = listener;
        mAuth.addAuthStateListener(mStateListener);
    }

    public void removeAuthStateListener() {
        mAuth.removeAuthStateListener(mStateListener);
    }

    //=================================================================
    // Sign Up Methods
    //=================================================================
    public void setSignUpOnCompleteListener(OnCompleteListener<AuthResult> listener) {
        mListenerSignUp = listener;
    }

    public void signUpWithEmailAndPassword(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(mListenerSignUp);
    }

    //=================================================================
    // Sign In Methods
    //=================================================================
    public void setSignInOnCompleteListener(OnCompleteListener<AuthResult> listener) {
        mListenerSignIn = listener;
    }

    public void signInWithEmailAndPassword(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(mListenerSignIn);
    }
}
